<?php
	require('conexion.php');
	
	$query="select id, name,price,description,category_id,created from products order by id";
	
	$resultado=$mysqli->query($query);
	
	//Para la paginación
	if (isset($_GET['pos']))
	  $ini=$_GET['pos'];
	else
	  $ini=1; 
?>

<html>
	<head>
	
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="bootstrap.css" type="text/css">
	 <style>
	   #pag {width:450px; font-size:12px; padding:50px;}
	   table th{background-color:#AED7FF;}
	 </style>
		<title>Read Products</title>
	</head>
	<body>
		<h1><center>Products</h1></center>
		<form name="new" action="nuevo.php">
		<button class="btn btn-primary">New product</button>
		</form>
		<p></p>
		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min"></script>
<!--Búsqueda de productos-->		
        <form name="busqueda" method="POST" action="busqueda.php">

			<table width="50%" align="right">
				<tr>
					<td ><font size=1>Search by name or description</font></td>
					<td ><input type="text" name="name" size="25" /></td>
				    <td ><font size=1><input type="submit" name="enviar"  value="Search" /></td>
			    </tr>
				</table>
				<br></br>
				<table width="50%" align="right">
				<tr>
					<td><font size=1>Date from...</td></font>
					<td><input type="text" name="fecha1" size="15" /></td>
				
					<td><font size=1>Date to...</td></font>
					<td><input type="text" name="fecha2" size="15" /></td>
					<td ><font size=1><input type="submit" name="enviar"  value="Search" /></td>
				</tr>
			</table>
		</form>

		<br>
			</br>
			<form method="POST" action="eliminarselect.php">
			<td ><font size=1 align="right"><input type="submit" name="export" class="btn btn-warning" value="Eliminar elementos seleccionados"  /></td>
			</form>


<br></br>
		
		<center><table border=1 width="70%">
		<div class="container">
		<table class="table table-bordered table-hover">
			<thead>
				<tr class="active">
				    <td><b>#</b></td>
					<td><b>Name</b></td>
					<td><b>Price</b></td>
					<td><b>Description</b></td>
					<td><b>Category</b></td>
					<td><b>Created</b></td>
					<th colspan="2">Actions</b></th>
					
				</tr>
				<tbody>
					<?php while($row=$resultado->fetch_assoc()){ ?>
					<form action="#" method="post">
					        <tr>
							<td align="center"><input type="checkbox" name="check" value=id> 
							</td>
							<td align="center"><?php echo $row['name'];?>
							</td>
							<td align="center"><?php echo $row['price'];?>
							</td>
							<td align="center"><?php echo $row['description'];?>
							</td>
							<td align="center"><?php echo $row['category_id'];?>
							</td>
							<td align="center"><?php echo $row['created'];?>
							</td>
							<td align="center">
								<a href="modificar.php?id=<?php echo $row['id'];?>">Modify</a>
							</td>
							<td align="center">
								<a href="eliminar.php?id=<?php echo $row['id'];?>">Delete</a>
							</td>
						</tr>
						
						</form>
					<?php } ?>
				</tbody>
			</table>
			</center>
			
			<form method="POST" action="excel.php">
			<td ><font size=1><input type="submit" name="export" class="btn btn-success" value="Exportar a excel" /></td>
			</form>
			<center>
			<div id="pag">
				<?php include('paginacion.php'); ?>
			</div>
			</center>
		</body>
	</html>	
	
