<?php  
 $connect = mysqli_connect("localhost", "root", "", "store");  
 $output = '';  
 if(isset($_POST["export"]))  
 {  
      $sql = "select id,name,description,category_id,created from products";  
      $result = mysqli_query($connect, $sql);  
      if(mysqli_num_rows($result) > 0)  
      {  
           $output .= '  
                <table class="table" bordered="1">  
                     <tr>  
                          <th>id</th>  
                          <th>name</th>  
                          <th>description</th>  
						  <th>category_id</th>  
                          <th>created</th>   
                     </tr>  
           ';  
           while($row = mysqli_fetch_array($result))  
           {  
                $output .= '  
                     <tr>  
                          <td>'.$row["id"].'</td>  
                          <td>'.$row["name"].'</td>  
                          <td>'.$row["description"].'</td>  
						  <td>'.$row["category_id"].'</td> 
						  <td>'.$row["created"].'</td> 
                     </tr>  
                ';  
           }  
           $output .= '</table>';  
           header("Content-Type: application/xls");   
           header("Content-Disposition: attachment; filename=download.xls");  
           echo $output;  
      }  
 }  
 ?>  