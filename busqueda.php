<?php 
	
	require('conexion.php');
	
	$name=$_POST['name'];
	$fecha1=$_POST['fecha1'];
	$fecha2=$_POST['fecha2'];
	
	$query=$query="select id, name,price,description,category_id,created from products where (name like '%$name%' or description like '%$name%') or (created<'$fecha2' and created<'$fecha1')";
	
	$resultado=$mysqli->query($query);
	
?>

<html>
	<head>
	
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	
		<title>Read Products</title>
	</head>
	<body>
		<h1>Products</h1>
		<button class="btn btn-primary"><a href="nuevo.php">New product</a></button>
		<p></p>
		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min"></script>
<!--Búsqueda de productos-->		
        <form name="busqueda" method="POST" action="busqueda.php">

			<table width="50%" align="right">
				<tr>
					<td ><font size=1>Search by name or description</font></td>
					<td ><input type="text" name="name" size="25" /></td>
				    <td ><font size=1><input type="submit" name="enviar"  value="Search" /></td>
			    </tr>
				</table>
				<br></br>
				<table width="50%" align="right">
				<tr>
					<td><font size=1>Date from...</td></font>
					<td><input type="text" name="fecha1" size="15" /></td>
				
					<td><font size=1>Date to...</td></font>
					<td><input type="text" name="fecha2" size="15" /></td>
					<td ><font size=1><input type="submit" name="enviar"  value="Search" /></td>
				</tr>
			</table>
		</form>



<br></br>
		
		<center><table border=1 width="70%">
		<div class="container">
		<table class="table table-bordered table-hover">
			<thead>
				<tr class="active">
					<td><b>Name</b></td>
					<td><b>Price</b></td>
					<td><b>Description</b></td>
					<td><b>Category</b></td>
					<td><b>Created</b></td>
					<th colspan="2">Actions</b></th>
					
				</tr>
				<tbody>
					<?php while($row=$resultado->fetch_assoc()){ ?>
							<tr>
							<td align="center"><?php echo $row['name'];?>
							</td>
							<td align="center"><?php echo $row['price'];?>
							</td>
							<td align="center"><?php echo $row['description'];?>
							</td>
							<td align="center"><?php echo $row['category_id'];?>
							</td>
							<td align="center"><?php echo $row['created'];?>
							</td>
							<td align="center">
								<a href="modificar.php?id=<?php echo $row['id'];?>">Modify</a>
							</td>
							<td align="center">
								<a href="eliminar.php?id=<?php echo $row['id'];?>">Delete</a>
							</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
			</center>
		</body>
	</html>	
	